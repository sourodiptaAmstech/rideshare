<?php

return array(

    'IOSUser'     => array(
        'environment' => env('IOS_USER_ENV', 'production'),
        //'certificate' => app_path().'/apns/user/user_live.pem',
        //'passPhrase'  => env('IOS_USER_PUSH_PASS', 'Appoets123$'),
        'certificate' => app_path().'/apns/user/UTurnUserDistributionPush.pem',
        'passPhrase'  => env('IOS_USER_PUSH_PASS', '1234'),
        'service'     => 'apns'
    ),
    'IOSProvider' => array(
        'environment' => env('IOS_PROVIDER_ENV', 'production'),
        //'certificate' => app_path().'/apns/provider/provider_live.pem',
        //'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', 'Appoets123$'),
        'certificate' => app_path().'/apns/provider/UTurnProviderDistributionPush.pem',
        'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', '1234'),
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' => env('ANDROID_USER_ENV', 'production'),
        'apiKey'      => env('ANDROID_USER_PUSH_KEY', 'yourAPIKey'),
        'service'     => 'gcm'
    ),
    'AndroidProvider' => array(
        'environment' => env('ANDROID_PROVIDER_ENV', 'production'),
        'apiKey'      => env('ANDROID_PROVIDER_PUSH_KEY', 'yourAPIKey'),
        'service'     => 'gcm'
    )

);