@extends('admin.layout.base')

@section('title', 'Promotions ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Promotions </h5>
                <a href="{{ route('admin.promotion.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Promotion</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Discount</th>
                            <th>Duration</th>
                            <th>Distance</th>
                            <th>Expiration</th>
                            <th>Status</th>
                            <th>Activated</th>
                            <th>Used Count</th>
                            <th>Content</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($promotions as $index => $promotion)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$promotion->discount}}</td>
                            <td>{{$promotion->driver_discout_criteria_time}}</td>
                            <td>{{$promotion->driver_discout_criteria_distance}}</td>
                            <td>{{date('d-m-Y',strtotime($promotion->expiration))}}</td>
                            <td>
                                @if(date("Y-m-d") <= $promotion->expiration)
                                    <span class="tag tag-success">Valid</span>
                                @else
                                    <span class="tag tag-danger">Expired</span>
                                @endif
                            </td>
                            <td>
                                @if($promotion->activated_fordriver == '1')
                                    <span class="tag tag-success">YES</span>
                                @else
                                    <span class="tag tag-danger">NO</span>
                                @endif
                            </td>
                            <td>{{promotion_used_count($promotion->id)}}</td>
                            <td>{{$promotion->content}}</td>
                            <td>
                                <form action="{{ route('admin.promotion.destroy', $promotion->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <a href="{{ route('admin.promotion.edit', $promotion->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Discount</th>
                            <th>Duration</th>
                            <th>Distance</th>
                            <th>Expiration</th>
                            <th>Status</th>
                            <th>Activated</th>
                            <th>Used Count</th>
                            <th>Content</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection