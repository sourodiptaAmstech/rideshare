<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Auth;
use DB;
use App\Document;
use App\ProviderDocument;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VehicleDocuments = Document::vehicle()->get();
        $DriverDocuments = Document::driver()->get();

        $Provider = \Auth::guard('provider')->user();

        return view('provider.document.index', compact('DriverDocuments', 'VehicleDocuments', 'Provider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'document' => 'mimes:jpg,jpeg,png,pdf',
            ]);
        if($request->has('app')) {
            $Provider = Auth::user();
        } else {
            $Provider = \Auth::guard('provider')->user();
        }
        
        $provider_id = $Provider->id;

        try {
            
            $Document = ProviderDocument::where('provider_id', $provider_id)
                ->where('document_id', $id)
                ->firstOrFail();

            $Document->update([
                    'url' => $request->document->store('provider/documents'),
                    'status' => 'ASSESSING',
                ]);

            if($request->has('app')) {
                return response()->json([
                    'done' => "Document updated successfully",
                ]);
            } else {
            return back();
            }

        } catch (ModelNotFoundException $e) {

            ProviderDocument::create([
                    'url' => $request->document->store('provider/documents'),
                    'provider_id' => $provider_id,
                    'document_id' => $id,
                    'status' => 'ASSESSING',
                ]);
            
        }

        if($request->has('app')) {
            return response()->json([
                'done' => "Document updated successfully",
            ]);
        } else {
        return back();
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * list provider document.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $VehicleDocuments = DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select provider_documents.status from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_status')
                    )
            ->where('type', 'VEHICLE')
            ->get();

        $DriverDocuments = DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select provider_documents.status from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_status')
                    )
            ->where('type', 'DRIVER')
            ->get();
        $Provider = Auth::user();

        return response()->json([
            'VehicleDocuments' => $VehicleDocuments,
            'DriverDocuments' => $DriverDocuments,
        ]);
    }
}
