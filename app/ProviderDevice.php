<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderDevice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id',
        'udid',
        'token',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    public function scopeProviderDeviceDetailsIos($query)
    {
        return $query->select('provider_devices.provider_id','provider_devices.token','provider_devices.udid')
        ->where('provider_devices.type', '=', 'ios');
    }
    public function scopeProviderDeviceDetailsAndroid($query)
    {
        return $query->select('provider_devices.provider_id','provider_devices.token','provider_devices.udid')
        ->where('provider_devices.type', '=', 'android');
    }
}
